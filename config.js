module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  baseBranches: ['main'],
  labels: ['renovate'],
  token: process.env.GITLAB_TOKEN,
  rebaseWhen: 'auto'
};
